FROM ruby:2.6.3

RUN apt-get -qq update
RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get -qq update
RUN apt-get install -y libqtwebkit4 libqt4-dev xvfb nodejs yarn
RUN gem install bundler -v 2.0.2

CMD ["bash"]

