A docker image that I like to use in my clients' CI workflows.
It is based off of the official Ruby image. It adds nodejs, yarn, qt4/qtwebkit and xvfb.

Specific Ruby versions can be used by selecting the correct tag, e.g. `ruby-2.4.5`.

